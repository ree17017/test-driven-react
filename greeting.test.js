const gretting = guest => `Hello, ${guest}!`;

describe('greeting()', () => {
  it('say hello', () => {
    expect(gretting('Jest')).toBe('Hello, Jest!');
  });
});
